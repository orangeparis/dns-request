
from dns_request.domains import DomainNames

custom_domain_filename= '../files/mydomains.txt'

def test_default_domains():

    size= 1000

    names= DomainNames.from_file(max=size)

    dn= names.clone()

    assert len(dn.domains) == size

    for i in range (400):
        name= dn.get_domain()
        print(name)

    return

def test_clones_share_domains():

    size= 1000

    names= DomainNames.from_file(max=size)

    dn1= names.clone()
    dn2= names.clone()

    assert len(dn1.domains) == size

    dn1.domains.append("hello.com")

    assert len(dn1.domains) == size +1
    assert len(dn2.domains) == size + 1

    return


def test_custom_domains():

    size= 100

    names = DomainNames.from_file(filename=custom_domain_filename, max=size)

    dn = names.clone()

    nb= len(dn.domains)

    assert nb <= size


    for i in range(nb):
        name = dn.get_domain()
        print(name)

    return


if __name__== "__main__":

    test_default_domains()
    test_custom_domains()
    test_clones_share_domains()


    print("done")

