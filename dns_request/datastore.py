"""


"""
import datetime
from .models import DnsHit

class DataStore(object):
    """


    """

    def store(self,severity='low',description="...",time=None ,duration= 0,tags = None,**kwargs):
        """

        :param severity:
        :param description:
        :param time:
        :param duration:
        :param tags:
        :return:
        """
        time= time or datetime.datetime.now()
        tags= tags or []

        rec= DnsHit(severity=severity,description=description,time=time,tags=tags,**kwargs)
        rec.save()

        return rec

