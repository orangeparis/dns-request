
#configure 

    cd /usr/share/packetbeat/bin
    cp /etc/packetbeat/* /usr/share/packetbeat/bin/


#launch

    cd /usr/share/bin && ./packetbeat -e -c packetbeat.yml -d "publish"


#send dashboard to es

    /usr/share/packetbeat/scripts/import_dashboards  -es http://elasticsearch:9200
    
    

#container can access host lan (etho)


    docker run --name capture-container --net=host capture-image


# mapped container

    docker run -d --name web1 -p 80:80 -p 8080:8080 jonlangemak/docker:web_container_80
    
    docker run -d --name web2 --net=container:web1 jonlangemak/docker:web_container_8080


#elastic request for dns

    type:dns AND NOT dns.question.name:elasticsearch. AND NOT dns.question.name:elasticsearch.home.
    
    
#packetbeat container

docker run --name packetbeat -d --net=host --env 'ELASTICHOSTS="\"elasticsearch:920\""' -d proteansec/packetbeat app:start