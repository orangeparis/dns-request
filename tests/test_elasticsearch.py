from datetime import datetime
from elasticsearch_dsl.connections import connections
from dns_request.models import DnsHit

from dns_request.datastore import DataStore

hosts= ['192.168.99.100']

def esconnection():
    # Define a default Elasticsearch client
    return connections.create_connection(hosts=hosts)


def test_create_sample(connection):


    # create the mappings in elasticsearch
    DnsHit.init()

    # create and save and article
    article = DnsHit(meta={'id': 42}, description='Dns request', tags=['test'])
    article.description = '''dns query two'''
    article.time = datetime.now()
    article.save()

    hit = DnsHit.get(id=42)
    print(article.time)
    print(article.description)
    print(article.tags)


    # Display cluster health
    print(connections.get_connection().cluster.health())


def test_datastore(connection):
    """


    :param connection:
    :return:
    """
    store = DataStore()

    for i in range(3):

        rec= store.store(severity="low",duration=i,description="label %d" % i)

    return






if __name__=="__main__":

    test_create_sample(esconnection())
    test_datastore(esconnection())

    print("Done")