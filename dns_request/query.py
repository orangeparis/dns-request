
import datetime
from elasticsearch import Elasticsearch

from elasticsearch_dsl import Search, Q

#elastic_hosts= ["192.168.1.26"]

elastic_hosts= ["192.168.99.100"]

client = Elasticsearch(hosts=elastic_hosts)


res = client.search(index="ips", body={"query": {"match_all": {}}})

res= client.search(index="ips", body=
    {
        "query": {
            "query_string" : {
                "fields" : ["severity", "time"],
                "query" : "severity:low",
                "use_dis_max" : True
            }
        }
    }
)




print("Got %d Hits:" % res['hits']['total'])
for hit in res['hits']['hits']:
    #print("%(timestamp)s %(author)s: %(text)s" % hit["_source"])
    print(hit['_source']['description'])


s = Search(using=client, index="ips") \
    .query("match", severity="low")

#
# s = Search(using=client, index="ips") \
#     .filter("term", category="search") \
#     .query("match", title="python")   \
#     .query(~Q("match", description="beta"))
#
# s.aggs.bucket('per_tag', 'terms', field='tags') \
#     .metric('max_lines', 'max', field='lines')
#
response = s.execute()

for hit in response:
    print(hit.meta.score, hit.time, hit.severity,hit.description)
#
# for tag in response.aggregations.per_tag.buckets:
#     print(tag.key, tag.max_lines.value)

