import socket
import time


from dnslib import DNSRecord , DNSQuestion, QTYPE
from dns_request.domains import DomainNames
from dns_request.datastore import DataStore

from .core import Requester

# class Requester(object):
#     """
#
#     """
#
#     def clone(self):
#         return self
#
#     def run(self):
#         raise NotImplementedError

storage= False


class DnsTxtRequester(Requester):
    """
        a requester to send dns request with dns type == TXT or WKS or NULL

    """
    def __init__(self,dns_ip,dns_port=53,duration=10,source=None,source_size=10000,timeout=5,verbose=0):
        """

        :param dns_ip:
        :param dns_port:
        :param source:
        :param duration:
        """
        self.dns_ip= dns_ip
        self.dns_port= dns_port
        self.duration= duration
        self.timeout= timeout
        self.verbose = verbose

        self.source= source or DomainNames.from_file(max=source_size)
        assert isinstance(self.source,DomainNames)

        self.count= 0
        self.datastore=DataStore()

    def clone(self):
        """

        :return: another identical requester but having its own source
        """
        cls= self.__class__
        new= cls(dns_ip=self.dns_ip,dns_port=self.dns_port,duration=self.duration,
                 timeout=self.timeout,verbose=self.verbose,source=self.source.clone())
        return  new


    def flood(self, pkt= None, domain=None):
        """

        :return:    send a request without waiting for response
        """
        ipv6= False

        if not pkt:
            domain= domain or self.source.get_domain()
            #q = DNSRecord.question(domain)
            q = DNSRecord(q=DNSQuestion(domain, QTYPE.TXT))
            pkt= q.pack()

        if ipv6:
            inet = socket.AF_INET6
        else:
            inet = socket.AF_INET

        sock = socket.socket(inet, socket.SOCK_DGRAM)
        sock.sendto(pkt, (self.dns_ip, self.dns_port))
        print("send a TXT flood dns packet for domain: %s" % domain)
        time.sleep(0.001)
        sock.close()


    def send(self, pkt= None, domain=None):
        """
            send a dns request and wait for response
        :return:
        """

        if not pkt:
            # choose a domain
            domain = domain or self.source.get_domain()
            #q = DNSRecord.question(domain)
            q = DNSRecord(q=DNSQuestion(domain, QTYPE.TXT))
        else:
            # build from pkt
            q= DNSRecord.parse(pkt)



        t0 = time.time()
        try:
            r = q.send(self.dns_ip, self.dns_port, timeout=self.timeout)
        except:
            r = None
            pass
        t1 = time.time()
        self.count = self.count + 1

        if self.verbose > 0 :
            print("domain: %s, time: %f, duration: %f" % (domain, t0, t1 - t0))
        completed = False
        if r:
            response = DNSRecord.parse(r)
            if response.rr:
                for i, a in enumerate(response.rr):
                    if self.verbose >= 2:
                        print("    data:%s" % response.rr[i].rdata)
                completed = True
            else:
                completed = False
                if self.verbose >=2 :
                    print('    no response')

        # store record
        if storage:
            self.datastore.store(severity="low", description='qname: %s' % q.q.qname, duration=t1 - t0, completed=completed)

        return t1-t0

    def run(self):
        """

        :return:
        """
        start= time.time()
        end= start + self.duration

        while 1:

            duration= self.send()
            t1= time.time()

            if t1 >= end:
                break

        # at the end
        effective_duration= t1 - start
        print("======================")
        print("%d request executed in %f, moy= %f" % (self.count, effective_duration, self.count / effective_duration))
        print("======================")


