"""

    # with cuckoo
    creating filter took: 1.8892431259155273
    filling took: 0.6887660026550293
    requesting took: 0.7000770568847656
    in set:  50096
    off set:  49904
    Done.


    # native
    creating filter took: 9.298324584960938e-06
    filling took: 0.2656440734863281
    requesting took: 0.29062986373901367
    in set:  50073
    off set:  49927
    Done.



"""
import time
from dns_request.domains import DomainNames
from dns_stat.filter import CuckooFilter
#from dns_stat.cuckoofilter import CuckooFilter


domain_filename= '../../files/top-1m.csv'


def test_one():
    """



    :return:
    """

    names= DomainNames.from_file(domain_filename,max=100)
    names2= DomainNames.from_file(domain_filename,max=200)

    ta= time.time()
    filter= CuckooFilter(1000000,2)
    tb= time.time()
    print("creating filter took:", tb-ta)

    # fill the filter
    t0= time.time()
    for n in range(100000):
        item = names.get_domain()
        if item not in filter:
            try:
                filter.add(item)
            except Exception as e:
                print("error while inserting %d" % n)
                raise
    t1= time.time()


    in_set_counter=0
    off_set_counter=0

    # request
    for n in range(100000):
        item= names2.get_domain()

        if item in filter:
            in_set_counter += 1
        else:
            off_set_counter +=1
    t2 =time.time()

    print("filling took: %s" % str(t1-t0))

    print("requesting took: %s" % str(t2-t1))

    print("in set: " , in_set_counter)
    print("off set: " , off_set_counter)


    return




if __name__=="__main__":


    test_one()
    print("Done.")


