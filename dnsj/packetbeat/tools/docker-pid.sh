#!/usr/bin/env bash

# get the pid of a container named web
# dockerp-pid.sh web

exec docker inspect --format '{{ .State.Pid }}' "$@"

