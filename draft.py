import random
import codecs
import base64
import Crypto.Cipher.AES
from Crypto.Cipher.AES import MODE_EAX

FLAG_LASTFRAGMENT = 1

CRYPTO_MODE= MODE_EAX


key = 'denise'
domains= ['orandns.info','orangedns.club']
text = 'Python rocks indeed!'



def bpad(text,length=16,char=b'='):
    """

    :param text:
    :param lenght:
    :return:
    """
    while len(text) % length != 0:
        text += char
    return text


def pad(text,lenght=16,char='='):
    """

    :param text:
    :param lenght:
    :return:
    """
    while len(text) % lenght != 0:
        text += char
    return text

def pad_rand(text,lenght=16):
    """

    :param text:
    :param lenght:
    :return:
    """
    while len(text) % lenght != 0:
        #char= chr(random.randrange(256))
        char = chr(random.randrange(128))
        text += char
    return text

def bpad_rand(text,lenght=16):
    """

    :param text:
    :param lenght:
    :return:
    """
    while len(text) % lenght != 0:
        #char= chr(random.randrange(256))
        #char = chr(random.randrange(128))
        char= bytes([random.randrange(128)])
        text += char
    return text



def encode64(inbuf):
    s = base64.encodebytes(inbuf)
    r = s.replace(b'+', b'-').replace(b'/', b'_').replace(b'\n', b'').strip()
    return r




def encode_packet(data, flags=0, seq=0):

    s= bytes([flags,seq,len(data)])
    s= s + data
    s= bpad_rand(s,16)
    s= cipher.encrypt(s)
    s= encode64(s)
    x= s.replace(b'=', b'')

    #
    # s = "%s%s%s%s" % (chr(flags),
    #                   chr(seq),
    #                   chr(len(data)),
    #                   data)
    #
    # s = pad_rand(s, 16)
    #
    # bs= bytes(s,'utf-8')
    #
    # s2 = cipher.encrypt(bs)
    # s3 = encode64(s2)
    # x = s3.replace(b'=', b'')
    return x


class Encoder:
    """Encode data in a stream of DNS queries.

    You have to call the constructor with a AES key (preferable 32
    bytes) and a list of domains which can be used to access the tunneling Server.

    Then just call encodeDns(packettype, flags, data) and you get a
    list of FQDNs to request for data transport.
    """

    def __init__(self, key, domains,channel=None):
        """

        :param key: str key for cypher
        :param domains: list of string
        """
        self._sequence_number = 0
        self.domains = domains
        #self.cipher = Crypto.Cipher.AES.new(pad(key, 16),mode=CRYPTO_MODE)
        padded_key = pad(key, 16)
        cipher_key = bytes(padded_key, 'utf-8')
        self.cipher = Crypto.Cipher.AES.new(cipher_key, mode=CRYPTO_MODE)

        self.maxfragmentsize = 72
        self.channel= channel or random.randint(0, 65535)

    def encode64(self, inbuf):
        #  return base64.encodestring(inbuf).replace('+', '-').replace('/', '_').replace('\n', '').strip()

        s= base64.encodebytes(inbuf)
        s2= base64.b64encode(inbuf)

        print(s==s2)

        r = s.replace(b'+', b'-').replace(b'/', b'_').replace(b'\n', b'').strip()

        t= codecs.decode(s)
        r= t.replace('+', '-').replace('/', '_').replace('\n', '').strip()
        return r
        # buffer= inbuf.replace(b'+', b'-').replace(b'/', b'_').replace(b'\n', b'').strip()
        # buffer= base64.encodebytes(buffer)
        # return base64.encodestring(inbuf).replace('+', '-').replace('/', '_').replace('\n', '').strip()

    def encodePacket(self, data, flags, seq):
        """Encode a single Packet of Data"""

        # s = "%s%s%s%s" % (chr(flags),
        #                   chr(seq),
        #                   chr(len(data)),
        #                   data)
        # s = padRand(s, 16)
        # s = self.cipher.encrypt(s)
        # s = self.encode64(s)
        # s = s.replace(b'=', b'')
        # return s
        xdata= codecs.encode(data,'utf-8')

        s = bytes([flags, seq, len(data)])
        s = s + xdata
        s = bpad_rand(s, 16)
        s = cipher.encrypt(s)
        s = self.encode64(s)
        #x = s.replace(b'=', b'')

        x = s.replace('=', '')

        return x


    def fragmentData(self, data, flags=0):
        """Break down a big chunk of bytes into packets and encode them."""
        self._sequence_number = 0
        cdata = zlib.compress(data)
        ret = []
        for i in range(0, len(cdata), self.maxfragmentsize):
            if i > len(cdata) - self.maxfragmentsize:
                # last packet
                ret.append(self.encodePacket(cdata[i:i + self.maxfragmentsize],
                                             flags | FLAG_LASTFRAGMENT,
                                             self._sequence_number))
            else:
                ret.append(self.encodePacket(cdata[i:i + self.maxfragmentsize],
                                             flags, self._sequence_number))
            self._sequence_number = (self._sequence_number + 1) % 256
        return ret



class Decoder:
    """

    """
    def __init__(self, key, domains):
        #self.cipher = Crypto.Cipher.AES.new(pad(key, 16),mode=CRYPTO_MODE)

        padded_key = pad(key, 16)
        cipher_key = bytes(padded_key, 'utf-8')
        self.cipher = Crypto.Cipher.AES.new(cipher_key, mode=CRYPTO_MODE)

        self.domains = domains
        self.datastore = {}
        self.seen_lastfragment = 1
        self.packetqueue = []

    def checkDataComplete(self,datastore=None):
        """

        :param datastore:
        :return:
        """
        datastore= datastore or self.datastore
        k = datastore.keys()
        for i in range(max(k)):
            if not i+1 in datastore:
            #if not datastore.has_key(i + 1):
                return None
        return 1

    def removeDomain(self, data):
        for x in self.domains:
            if data.endswith(x):
                return data[:data.find(x)]


    def decode64(self, inbuf):
        """

        :param inbuf:
        :return:
        """
        s= inbuf.replace(b'-', b'+').replace(b'_', b'/')
        s2= base64.decodebytes(s)
        #s1= s.decode(s,'utf-8')
        #r= s.replace(b'-', b'+').replace(b'_', b'/')
        return s2
        #return base64.decodestring(inbuf.replace('-', '+').replace('_', '/'))

    def decodePacket(self, inbuf):



        s = bpad(inbuf, 4)
        #s = self.decode64(s + b'=')
        s = self.decode64(s)
        s = self.cipher.decrypt(s)

        flags= s[0]
        seq= s[1]
        plen=s[2]
        data=s[3:plen +3]
        return (data, flags, seq)

        # flags = ord(s[0])
        # seq = ord(s[1])
        # plen = ord(s[2])
        # data = s[3:plen + 3]
        # return (data, flags, seq)

    def decodeData(self, data):
        for p in data:
            payload, flags, serial = self.decodePacket(p)
            self.datastore[serial] = (payload, flags, serial)
            if flags & FLAG_LASTFRAGMENT == FLAG_LASTFRAGMENT:
                self.seen_lastfragment = 1
        if self.seen_lastfragment == 1:
            if self.checkDataComplete():
                return self.packetsToData(), (flags ^ FLAG_LASTFRAGMENT)
        return None



# from Crypto.Cipher import DES
# key = b'abcdefgh'
# def pad(text):
#         while len(text) % 16 != 0:
#             text += b'='
#         return text
# des = DES.new(key, DES.MODE_ECB)
# text = b'Python rocks!'
# padded_text = pad(text)
# encrypted_text = des.encrypt(text)


# text= b'pythonrocks!'
# from Crypto.Cipher import AES
# from Crypto.Random import get_random_bytes
# key = get_random_bytes(16)
# cipher = AES.new(key, AES.MODE_EAX)
# ciphertext, tag = cipher.encrypt_and_digest(text)
#file_out = open("encrypted.bin", "wb")

#[ file_out.write(x) for x in (cipher.nonce, tag, ciphertext) ]





padded_key= pad(key, 16)
cipher_key= bytes(padded_key,'utf-8')
cipher = Crypto.Cipher.AES.new(cipher_key,mode=CRYPTO_MODE)

text = 'Python rocks indeed!'
padded_text = pad(text)


btext= codecs.encode(text,'utf-8')

bpadded= codecs.encode(padded_text,'utf-8')

#encrypted_text = des.encrypt(btext)

#encrypted_text = cipher.encrypt(btext)

encoded_packet= encode_packet(btext)

print(encoded_packet)



encoder= Encoder(key,domains=domains)
decoder= Decoder(key,domains=domains)



encoded_packet= encoder.encodePacket(text,0,0)

encoded_packet= codecs.encode(encoded_packet,'utf-8')

decoded= decoder.decodeData([encoded_packet])



print()

