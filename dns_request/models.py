"""

    elastic search model for a dns reccord

"""
from elasticsearch_dsl import DocType, Date, Integer, Keyword, Text,Boolean

# Define a default Elasticsearch client
#connections.create_connection(hosts=['192.168.99.100'])

class DnsHit(DocType):
    """

    """

    severity= Text()
    description= Text()

    time= Date()
    duration= Integer()

    # title = Text(analyzer='snowball', fields={'raw': Keyword()})
    # body = Text(analyzer='snowball')
    tags = Keyword()
    # published_from = Date()
    # lines = Integer()
    completed= Boolean()

    class Meta:
        index = 'dnsj'

    def save(self, ** kwargs):
        return super(DnsHit, self).save(** kwargs)

    # def is_published(self):
    #     return datetime.now() < self.published_from
