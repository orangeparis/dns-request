"""
    DomainNames

    a catalog of domain names


    usage:

        dnref= DomainNames.from_file(max=10000)

        dn= dnref.clone()

        name= n.get_domain()

"""
import random

# a list of 1 million of internet top domains ( csv   1,domain.com )
domain_name_file= '../files/top-1m.csv'


class DomainNames(object):
    """


    """
    def __init__(self,domains):
        """

        :param source: string name of domain file ( 1,google.com)
        :param limit:
        """
        self.domains= domains
        self._hits= {}

    @classmethod
    def from_file(cls,filename=domain_name_file ,max=1000):
        """

        :param max:
        :return:
        """
        f_domains = open(filename)

        domains = []

        for index, line in enumerate(f_domains.readlines()):
            if line :
                if  ',' in line:
                    # csv comma separated line
                    _dummy, domain = line.split(',', 1)
                    domain= domain.strip().strip('\n')

                else:
                    # assume 1 line -> 1 domain
                    domain= line.strip().strip('\n')

                domains.append(domain)

            if len(domains) >= max:
                break

        return cls(domains)


    def get_domain(self):
        """

        :return:
        """
        index= random.randint(0,len(self.domains)-1)
        if not index in self._hits:
            self._hits[index]= 1
        else:
            self._hits[index] = self._hits[index] + 1

        return self.domains[index]


    def clone(self):
        """


        :return: another requester with same parameters
        """
        cls= self.__class__
        return cls(self.domains)




if __name__== "__main__":

    names= DomainNames.from_file(max=1000)

    dn= names.clone()

    for i in range (400):
        name= dn.get_domain()
        print(name)


    print("done")

