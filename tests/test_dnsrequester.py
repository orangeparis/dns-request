import time
from elasticsearch_dsl.connections import connections
from dns_request.models import DnsHit


from dns_request.requesters.dns_requester import DnsRequester

from dns_request.manager import RequestManager


dns_ip= '192.168.1.26'
#dns_ip = '192.168.1.1'
#dns_ip = '8.8.8.8'
dns_port = 53

hosts= ['192.168.99.100']


# create elasticsearch connection
es= connections.create_connection(hosts=hosts)
# send mappings to elasticsearch
DnsHit.init()


def test_flood():
    """

    :return:
    """
    # create a dns requester model
    dns_requester= DnsRequester(dns_ip=dns_ip,dns_port=dns_port,duration=600,timeout=4,source_size=1000)

    t0= time.time()
    for i in range(10000):
        t = dns_requester.flood()
    t1= time.time()
    print("flood: 10000 requests in %f" % (t1-t0))
    return


def test_run_once():
    """

    :return:
    """
    # create a dns requester model
    dns_requester = DnsRequester(dns_ip=dns_ip, dns_port=dns_port, duration=10, timeout=4, source_size=1000)

    t = dns_requester.send()


if __name__== "__main__":
    """

    """
    test_run_once()

    #test_flood()

    print("Done")
