

special settings
================

see: https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html

The vm_max_map_count setting must be set via docker-machine:

docker-machine ssh
sudo sysctl -w vm.max_map_count=262144
