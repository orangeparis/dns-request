"""


A cuckoo filter supports following operations:

add(item): insert an item to the filter
contains(item): return if item is already in the filter. Note that this method may return false positive results like Bloom filters
remove(item): delete the given item from the filter. Note that to use this method, it must be ensured that this item is in the filter (e.g., based on records on external storage); otherwise, a false item may be deleted.
size(): return the total number of items currently in the filter
#SizeInBytes(): return the filter size in bytes


"""


class CuckooFilter(object):
    """
        a dummy bloom filter based on a set


        def __init__(self, filter_capacity,
                 item_fingerprint_size, num_swaps=500, bucket_size=4):


    """

    def __init__(self, filter_capacity=0,
                 item_fingerprint_size=0, num_swaps=500, bucket_size=4):
        """
            not used. (for compatibility with cuckoo filter)

        """
        self.container= set()

    def __contains__(self, item):
        """

        :param item_to_test:
        :return:
        """
        if item not in self.container:
            # item is not in the container ( guaranteed)
            return False
        else:
            # question is in the set ( maybe a False positive ( 3% )
            return True

    def contains(self,item):
        """

        :param question:
        :return:
        """
        if item not in self.container:
            # question is not in the container ( guaranted)
            return False
        else:
            # question is in the set ( maybe a False positive ( 3% )
            return True


    def add(self, item):
        """

        :param item:
        :return:
        """
        if item in self:
            # already in set , we dont add it , so return False
            return False
        # not in container : we add it , so return True
        self.container.add(item)
        return True



    def remove(self, item):
        """

        :param item:
        :return:
        """
        if item in self:
            self.container.remove(item)
            return True
        return False

        # if self.contain(item):
        #     self.container.remove(item)


    def size(self):
        """

        :return:
        """
        return len(self.container)



    # def close(self):
    #     """
    #
    #     :return:
    #     """
    #     # close the bloom filter
    #     del self.container
    #     self.container= None

