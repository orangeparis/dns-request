import time


from dnslib import DNSRecord
from .domains import DomainNames
from .datastore import DataStore


class Requester(object):
    """

    """

    def clone(self):
        return self

    def run(self):
        raise NotImplementedError



class DnsRequester(Requester):
    """


    """
    def __init__(self,dns_ip,dns_port=53,duration=10,source=None,source_size=10000,timeout=5):
        """

        :param dns_ip:
        :param dns_port:
        :param source:
        :param duration:
        """
        self.dns_ip= dns_ip
        self.dns_port= dns_port
        self.duration= duration
        self.timeout= timeout

        self.source= source or DomainNames.from_file(max=source_size)
        assert isinstance(self.source,DomainNames)

        self.count= 0
        self.datastore=DataStore()

    def clone(self):
        """

        :return: another identical requester but having its own source
        """
        cls= self.__class__
        new= cls(dns_ip=self.dns_ip,dns_port=self.dns_port,source=self.source.clone())
        return  new


    def run(self):
        """

        :return:
        """
        start= time.time()
        end= start + self.duration

        while 1:

            # choose a domain
            domain = self.source.get_domain()

            q = DNSRecord.question(domain)
            qname= q.q.qname

            t0 = time.time()
            try:
                r = q.send(self.dns_ip, self.dns_port, timeout=self.timeout)
            except:
                r= None
                pass
            t1 = time.time()
            self.count= self.count +1

            print("domain: %s, time: %f, duration: %f" % (self.source.get_domain(),t0 ,t1 - t0))
            completed= False
            if r:
                response = DNSRecord.parse(r)
                if response.rr:
                    for i, a in enumerate(response.rr):
                        print("    data:%s" % response.rr[i].rdata)
                    completed= True
                else:
                    completed= False
                    print('    no response')

            # store record
            self.datastore.store(severity="low",description= 'qname: %s' % qname,duration= t1-t0,completed=completed)

            if t1 >= end:
                break

        # at the end
        effective_duration= t1 - start
        print("======================")
        print("%d request executed in %f, moy= %f" % (self.count, effective_duration, self.count / effective_duration))
        print("======================")


