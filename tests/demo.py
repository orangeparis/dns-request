
from elasticsearch_dsl.connections import connections
from dns_request.models import DnsHit


from dns_request.requesters.dns_requester import DnsRequester
from dns_request.requesters.denise_requester.fake_denise import DeniseRequester

from dns_request.requesters.txt_requester import DnsTxtRequester


from dns_request.manager import RequestManager



#dns_ip= '192.168.1.26'
#dns_ip = '192.168.1.1'
#dns_ip = '8.8.8.8'
#dns_ip = '192.168.1.51'
#dns_ip = "127.0.0.1"
#dns_ip= "192.168.1.21"

dns_port = 53

# target vm
TARGET = "VM"
dns_ip= '192.168.99.100'
hosts= ['192.168.99.100']


#TARGET = "fox"
#TARGET= "proxy"
#TARGET= "digital-ocean"
DURATION= 600


if TARGET == "fox" :
    # target fox
    dns_ip= '192.168.1.26'
    hosts= ['192.168.1.26']
elif TARGET == "digital-ocean":
    # target
    dns_ip = 'dnsguard.duckdns.org'
    hosts = ['dnsguard.duckdns.org']
elif TARGET == "proxy":
    # target
    dns_ip = '127.0.0.1'
    dns_port = 5350
    hosts = ['192.168.99.100']




print("running requesters to ip:%s , hosts:%s" % (dns_ip,str(hosts)))


# create elasticsearch connection
#es= connections.create_connection(hosts=hosts)
# send mappings to elasticsearch
#DnsHit.init()

verbose=2
source_size = 1000

# create a dns requester model
dns_requester= DnsRequester(dns_ip=dns_ip,dns_port=dns_port,duration=DURATION,timeout=4,source_size=1000,verbose=verbose)

# create a dns requester ( for TXT dns type -
dns_txt_requester= DnsTxtRequester(dns_ip=dns_ip,dns_port=dns_port,duration=DURATION,timeout=4,source_size=1000,verbose=verbose)



# create a fake denise requester
denise_requester= DeniseRequester(dns_ip=dns_ip,dns_port=dns_port,duration=DURATION,timeout=2,verbose=verbose)





# create request manager
manager = RequestManager()

# add some instances of requester to manager
manager.add_requester(dns_requester, number=4)

# add instance od denise requester
manager.add_requester(denise_requester, number=2)


# add some instances of requester to manager
manager.add_requester(dns_txt_requester, number=1)

# start request manager
total_request = manager.run()

print("============ TOTAL REQUEST : %d    ======= ", total_request)

print("Done")
