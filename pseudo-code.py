import time



main_domains={}  # a dictionary of domain:count

#slice_domains={}


# list of samples, one sample for each slice_period
# the content is domains dictionary
samples= []

def receive():
    # receive dns packet

    pkt= 'dns:question.domain'
    while 1 :
        yield pkt




def extract_question(pkt):
    """

    :param pkt:
    :return:
    """
    return pkt[3:]


def split_question(question):
    """

    :param question: string eg host.domain
    :return:
    """
    return question.split('.')



def add_question(question):
    """

    :param question:
    :return:
    """
    #store question in question set


def add_to_set(question):
    """

    :param question:
    :return:
    """

def is_in_set(question):


    # the question is not in the set ( sure ) :  it is a new question
    return False


def handle_slice_period(slice_domain):
    """

        extract from collected domain  the domains with higher counts
    :return:
    """

    # select domains with higher counts
    high_counts= []

    # for each domain in high_counts
    #for domain in
      # update main domains


def handle_main_period():
    """

        extract from collected domain  the domains with higher counts
    :return:
    """

    # select domains with higher counts
    high_counts = []



slice_index= 0

slice_period = 5
main_period= 60


while 1:


    t0= time.time()
    t1= t0 + slice_period

    slice_domains = {}
    while t1:
        # the slice loop

        pkt= receive()
        question = extract_question(pkt)

        if is_in_set (question):

            # we already receive this question
            pass
        else:

            # add question to the set
            add_to_set(question)

            # it is a new question : increment domain count
            host,domain= split_question(question)
            if not domain in slice_domains:
                slice_domains[domain]= 1
            else:
                slice_domains[domain] = slice_domains[domain] + 1


        # check end of scan period
        if time.time() <= t1:
            continue

    # end of a scan period
    handle_slice_period(slice_domains)















