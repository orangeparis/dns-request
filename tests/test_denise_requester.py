import time


from dns_request.requesters.denise_requester.fake_denise import DeniseRequester

from dns_request.manager import RequestManager


dns_ip= '192.168.99.100'
#dns_ip = '192.168.1.1'
#dns_ip = '8.8.8.8'
dns_port = 53


def test_flood():
    """

    :return:
    """
    # create a dns requester model
    dns_requester= DeniseRequester(dns_ip=dns_ip,dns_port=dns_port,duration=600,timeout=4)

    t0= time.time()
    for i in range(10000):
        t = dns_requester.flood()
    t1= time.time()
    print("flood: 10000 requests in %f" % (t1-t0))
    return


def test_send():
    """

    :return:
    """
    # create a dns requester model
    dns_requester = DeniseRequester(dns_ip=dns_ip, dns_port=dns_port, duration=10, timeout=2)

    t = dns_requester.run()


if __name__== "__main__":
    """

    """
    test_send()
    #test_flood()

    print("Done")