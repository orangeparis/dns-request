
import time
import random
import heapq


class BloomFilter(object):
    """


    """
    def __init__(self):
        """


        """
        self.container= set()


    def is_in_set(self,question):
        """

        :param question:
        :return:
        """
        if question not in self.container:
            # question is not in the container ( guaranted)
            return False
        else:
            # question is in the set ( maybe a False positive ( 3% )
            return True


    def add(self,question):
        """

        :param question:
        :return:
        """
        if self.is_in_set(question):
            # already in set , we dont add it , so return False
            return False
        else:
            # not in container : we add it , so return True
            self.container.add(question)
            return True


    def remove(self,question):
        """

        :param question:
        :return:
        """
        if self.is_in_set(question):
            self.container.remove(question)


    def close(self):
        """

        :return:
        """
        # close the bloom filter
        del self.container
        self.container= None



def receive():
    # receive dns packet


    while 1 :
        host = random.choice(["host1", "host2", "host3","host4","google"])
        if host == "google":
            domain= "com"
        else:
            domain = random.choice(["com", "fr", "io"])

        pkt = 'dns:%s.%s' % (host, domain)

        yield pkt




def extract_question(pkt):
    """

    :param pkt:
    :return:
    """
    return pkt[4:]


def split_question(question):
    """

    :param question: string eg host.domain
    :return:
    """
    return question.split('.')



def collect(period):


    bloom = BloomFilter()
    domains= {}

    t0= time.time()
    t1 = t0 + period


    count =0
    for packet in receive():

        count += 1
        question = extract_question(packet)


        inserted = bloom.add(question)

        if inserted:

            # this is a new question
            host,domain= split_question(question)

            # add connter for the domain
            if not domain in domains:
                domains[domain]= 1
            else:
                domains[domain] = domains[domain] +  1
        else:
            # the question has already been meet: we can ignore it
            pass

        # check end of period
        if time.time() >= t1 :
            break

    # end of collected period
    print(count)
    print(domains)


    #find the most hit
    heap= []
    heapq.heapify(heap)
    for domain,n in domains.items():
        heapq.heappush(heap,(n,domain))


    #print("largest")
    #print(heapq.nlargest(2,heap))



    return heap


if __name__ == "__main__":


    domains= collect(1)
    print("largest")
    print(heapq.nlargest(2,domains))


    print('done')


