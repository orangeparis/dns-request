import threading


from dns_request.requesters.core import Requester


class RequestManager(object):
    """


    """
    def __init__(self):
        """

        :param nb_domains:
        :param duration:
        :param nb_requester:
        """

        self.requesters=[]
        self.tasks=[]
        self.total_count= 0


    def add_requester(self,requester,number=1):
        """
            add a requester ( or more )

        :param requester:
        :param number:
        :return:
        """
        assert isinstance(requester, Requester)
        for i in range(number):
            self.requesters.append(requester.clone())


    def run(self):
        """

        :return:
        """
        # # build requesters
        # self.requesters = [DnsRequester(dns_ip=self.dns_ip,dns_port=self.dns_port, source=self.source.clone(), duration=self.duration) for i in
        #               range(self.nb_requester)]

        # create tasks for requesters
        self.tasks = [threading.Thread(target=r.run) for r in self.requesters]

        # start requesters
        for t in self.tasks:
            t.start()

        # wait for task to complete
        for t in self.tasks:
            t.join()

        self.total_count = 0
        for r in self.requesters:
            self.total_count += r.count

        return self.total_count
